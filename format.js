 /**
 *
 * @api {get} /user/:id Get One Data
 * @apiName RequestOneData
 * @apiGroup JSON-Format
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *			"data":{
 *				"userId":1,
 *				"userFullName":"Nur Yaumi",
 *				"userName":"yaumianwar",
 *				"userEmail":"yaumi@mail.com",
 *				"userGender":2,
 *				"userRole":1,
 *				"userLastLoginTime":"2017-01-08T09:55:41+08:00",
 *				"userRegisteredAt":"2017-01-01T00:32:05+08:00",
 *				"userActive":true
 *			},
 *			"included":[
 *				{
 *					"userRole":[{
 *						"roleId":1,
 *						"roleName":"admin"
 *					}]
 *				},
 *				{
 *					"userGender":[{
 *						"gndrId":2,
 *						"gndrName":"female"
 *					}]
 *				}
 *			]
 *     }
 */

 /**
 *
 * @api {get} /user Get All Data
 * @apiName RequestAllData
 * @apiGroup JSON-Format
 *
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *		   "data": [
 *      		 {
 *           		"userActive": true,
 *           		"userEmail": "yaumi@mail.com",
 *           		"userFullName": "Nur Yaumi",
 *           		"userGender": 2,
 *           		"userId": 1,
 *           		"userLastLoginTime": "2017-01-08T09:55:41+08:00",
 *           		"userName": "yaumianwar",
 *           		"userRegisteredAt": "2017-01-01T00:32:05+08:00",
 *           		"userRole": 1
 *       		},
 *       		{
 *           		"userActive": true,
 *           		"userEmail": "ngurajeka@mail.com",
 *           		"userFullName": "Ady Rahmat",
 *           		"userGender": 1,
 *           		"userId": 2,
 *           		"userLastLoginTime": "2017-01-07T16:17:56+08:00",
 *           		"userName": "ngurajeka",
 *           		"userRegisteredAt": "2017-01-01T13:15:28+08:00",
 *           		"userRole": 2
 *       		},
 *       		{
 *           		"userActive": true,
 *           		"userEmail": "rudinidd@mail.com",
 *           		"userFullName": "Andi Rudini",
 *           		"userGender": 1,
 *           		"userId": 5,
 *           		"userLastLoginTime": "0001-01-01T00:00:00Z",
 *           		"userName": "andirudini",
 *           		"userRegisteredAt": "2017-01-07T16:36:35+08:00",
 *           		"userRole": 2
 *       		}
 *   		],
 *   		"included": [
 *				{
 *           		"userRole": [
 *               		{
 *                   		"roleId": 1,
 *                   		"roleName": "admin"
 *               		},
 *               		{
 *                   		"roleId": 2,
 *                   		"roleName": "user"
 *               		},
 *              		{
 *                   		"roleId": 2,
 *                   		"roleName": "user"
 *               		}
 *           		]
 *       		},
 *       		{
 *           		"userGender": [
 *               		{
 *                   		"gndrId": 2,
 *                   		"gndrName": "female"
 *               		},
 *               		{
 *                   		"gndrId": 1,
 *                   		"gndrName": "male"
 *               		},
 *               		{
 *                   		"gndrId": 1,
 *                   		"gndrName": "male"
 *               		}
 *           		]
 *       		}
 *   		]
 *		}
 *
 */

 /**
 *
 * @api {get} /error Error Message
 * @apiName ErrorMessage
 * @apiGroup JSON-Format
 *
 *
 * @apiError (409) NotAuthenticated Only authenticated user can read the data.
 * @apiError (409) RequiredAdmin Only authenticated admin can read the data.
 * @apiError (404) DataNotFound Data was not found.
 *
 * @apiErrorExample NotAuthenticated:
 *  HTTP/1.1 409 Conflit
 *	{
 *		"error": {
 *			"message": "Unauthorized"
 *		}
 *  }
 *
 * @apiErrorExample RequiredAdmin:
 *  HTTP/1.1 409 Conflit
 *	{
 *		"error": {
 *			"message": "Required admin"
 *		}
 *  }
 *
 * @apiErrorExample DataNotFound:
 *  HTTP/1.1 404 Not Found
 *	{
 *		"error": {
 *			"message": "Data not found"
 *		}
 *  }
 */

 /**
 *
 * @api {get} /validation Validation Error
 * @apiName ValidationError
 * @apiGroup JSON-Format
 *
 *
 * @apiError (403) ValidationError The input is not acceptable.
 * @apiErrorExample ValidationError:
 *  HTTP/1.1 403 Conflit
 *	{
 *		"error": {
 *			"message": "Invalid Data",
 *			"errors": [
 *				{
 *					"field": "username",
 *					"message": "Usename cannot be empty"
 *				},
 *				{
 *					"field": "password",
 *					"message": "Password cannot be empty"
 *				}
 *			]
 *		}
 *   }
 */
